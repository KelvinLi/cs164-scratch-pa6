'use strict';

var _ = require('./underscore.js');


/** The Node class represents a node in a Bayes Net. */
var Node = function(name, domain) {
  this.name = name;
  this.domain = domain;
  /* A list of unique parent nodes. */
  this.parents = [];
  /* A list of unique child nodes. */
  this.children = [];
}

Node.prototype.add_parent = function(parent) {
  this.add_parents([parent]);
}

Node.prototype.add_parents = function(parents) {
  this.parents = _.union(this.parents, parents);
}

Node.prototype.add_child = function(child) {
  this.add_children([child]);
}

Node.prototype.add_children = function(children) {
  this.children = _.union(this.children, children);
}

Node.prototype.remove_parent = function(parent) {
  this.remove_parents([parent]);
}

Node.prototype.remove_parents = function(parents) {
  this.parents = _.difference(this.parents, parents);
}

Node.prototype.remove_child = function(child) {
  this.remove_children([child]);
}

Node.prototype.remove_children = function(children) {
  this.children = _.difference(this.children, children);
}


/** The CPT class represents a conditional probability table for a node in a
 *  Bayes Net. */
var CPT = function(node, parents) {
  this.node = node;
  /* A list of parent nodes of node. */
  this.parents = parents;
  /* Map from lists of evidence values to conditional probability
     distributions. */
  this.cpt = {};
}

/** Returns a list of values from the evidence used to select the correct
 *  conditional probability distribution from the CPT. The evidence is
 *  represented as an object whose properties and values are the names and
 *  values of the parent nodes. */
CPT.prototype.extract_evidence = function(evidence) {
  return _.map(this.parents, function(parent) {
    return evidence[parent.name];
  });
}

/** Adds the distribution of the values of the node given the evidence to the
 *  CPT. */
CPT.prototype.add = function(evidence, distribution) {
  this.cpt[this.extract_evidence(evidence)] = normalize(distribution);
}

/** Returns the conditional probability that the node has the specified value
 *  given the evidence. */
CPT.prototype.get_cond_prob = function(evidence, value) {
  var evidenceValues = this.extract_evidence(evidence);
  return this.cpt[evidenceValues][value];
}


/** The BayesNet class represents a Bayes Net. */
var BayesNet = function(nodes, cpts) {
  this.nodes = nodes;
  this.cpts = cpts;
  /** Map from node names to nodes. */
  this.nodeMap = {};
  /** Map from node names to CPTs. */
  this.cptMap = {};
  var bn = this;
  _.each(nodes, function(node) {
    bn.nodeMap[node.name] = node;
  });
  _.each(cpts, function(cpt) {
    bn.cptMap[cpt.node.name] = cpt;
    /* Add parent and child links between the nodes. */
    cpt.node.add_parents(cpt.parents);
    _.each(cpt.parents, function(parent) {
      parent.add_child(cpt.node);
    });
  });
}

BayesNet.prototype.get_node = function(nodeName) {
  return this.nodeMap[nodeName];
}

BayesNet.prototype.get_cpt = function(node) {
  return this.cptMap[node.name];
}

/** Returns a list of the values of the specified nodes, in order. */
BayesNet.prototype.get_values = function(nodes) {
  return _.map(nodes, function(node) { return node.value; });
}

/** Returns the Markov blanket of the node. */
BayesNet.prototype.get_markov_blanket = function(node) {
  return _.without(_.union(node.parents, node.children,
    _.flatten(_.map(node.children, function(child) {
      return child.parents;
    }))
  ), node);
}

BayesNet.prototype.remove_node = function(nodeName) {
  var node = this.get_node(nodeName);
  var cpt = this.get_cpt(node);
  _.each(cpt.parents,function(parent){
    node.remove_parent(parent);
    parent.remove_child(node);
  });
  this.remove_cpt(node);
  this.nodes = _.without(this.nodes, node);
  delete this.nodeMap[nodeName];
}

BayesNet.prototype.remove_cpt = function(node) {
  var cpt = this.get_cpt(node);
  this.cpts = _.without(this.cpts, cpt);
  delete this.cptMap[node.name];
}

/** Initializes the specified nodes to random values in their domains. */
BayesNet.prototype.set_random_values = function(nodes) {
  _.each(nodes, function(node) {
    var distribution = {};
    _.each(node.domain, function(value) {
      distribution[value] = 1;
    });
    node.value = get_sample(distribution);
  });
}

/** Sets the values of the evidence nodes according to the given evidence. */
BayesNet.prototype.set_evidence = function(evidence) {
  var bn = this;
  _.each(evidence, function(value, key) {
    var node = bn.nodeMap[key];
    node.value = value;
  });
}

/** Returns the probability that the node has the specified value given the
 *  values of the parent nodes. */
BayesNet.prototype.prob_given_parents = function(node, value) {
  var evidence = {}
  _.each(node.parents, function(parent) {
    evidence[parent.name] = parent.value;
  });
  return this.get_cpt(node).get_cond_prob(evidence, value);
}

/** Returns the probability that the node has the specified value given the
 *  values of the nodes in the Markov Blanket. */
BayesNet.prototype.prob_given_markov_blanket = function(node, value) {
  return this.distr_given_markov_blanket(node)[value];
}

/** Returns the probability distribution of the node given the values of the
 *  nodes in the Markov Blanket. */
BayesNet.prototype.distr_given_markov_blanket = function(node) {
  var bn = this;
  /* Save the current value of the node. */
  var nodeValue = node.value;
  var distribution = {};
  _.each(node.domain, function(k) {
    node.value = k;
    var prob = bn.prob_given_parents(node, k);
    var childProbs = _.map(node.children, function(child) {
      return bn.prob_given_parents(child, child.value);
    });
    prob = _.reduce(childProbs, function(memo, p) { return memo * p; }, prob);
    distribution[k] = prob;
  });
  /* Restore the original value of the node. */
  node.value = nodeValue;
  return normalize(distribution);
}


/** Normalizes the distribution, which is represented as an object. */
function normalize(distribution) {
  var values = _.values(distribution)
  var sum = _.reduce(values, function(memo, num) { return memo + num; }, 0);
  /* Avoid dividing by zero */
  if (sum === 0 || sum === 1) {
    return distribution;
  } else {
    var normalizedDistribution = {};
    _.each(distribution, function(value, key) {
      normalizedDistribution[key] = distribution[key] / sum;
    })
    return normalizedDistribution;
  }
}

/** Returns a sample from the specified distribution. */
function get_sample(distribution) {
  distribution = normalize(distribution);
  var r = Math.random();
  for (var key in distribution) {
    r -= distribution[key];
    if (r < 0) {
      return key;
    }
  }
}

/** Returns the probability distribution of the query node given the evidence
 *  in the Bayes Net by running the Gibbs sampling algorithm for the specified
 *  number of iterations. The evidence is represented as an object whose
 *  properties and values are the names and values of the evidence nodes.
 *  Caches intermediate values if caching is true. */
function gibbs_ask(queryNode, evidence, bayesNet, iterations, caching) {
  if (! _.contains(bayesNet.nodes, queryNode)) {
    console.log("Error: Bayes Net does not contain query node."); // Error message
    return {};
  }
  var counts = {};
  _.each(queryNode.domain, function(value) {
    counts[value] = 0;
  });
  var evidenceNodes = _.map(_.keys(evidence), function(nodeName) {
    return bayesNet.get_node(nodeName);
  });
  var nonevidenceNodes = _.difference(bayesNet.nodes, evidenceNodes);
  bayesNet.set_evidence(evidence);
  bayesNet.set_random_values(nonevidenceNodes);
  if (caching) {
    /* Store the most recent probability distribution given Markov Blanket. */
    var cache = {}
    var value;
    for (var iter = iterations; iter > 0; iter--) {
      _.each(nonevidenceNodes, function(node) {
        if (cache[node.name] === undefined) {
          /* Cache this value to save computation of conditional probability
             distribution if values of nodes in Markov Blanket remain the same. */
          cache[node.name] = bayesNet.distr_given_markov_blanket(node);
        }
        value = get_sample(cache[node.name]);
        if (value != node.value) {
          /* Invalidate the cache for all nodes whose Markov Blanket contain node. */
          _.each(bayesNet.get_markov_blanket(node), function(node) {
            cache[node.name] = undefined;
          });
          node.value = value;
        }
        counts[queryNode.value] += 1;
      });
    }
  } else {
    for (var iter = iterations; iter > 0; iter--) {
      _.each(nonevidenceNodes, function(node) {
        node.value = get_sample(bayesNet.distr_given_markov_blanket(node));
        counts[queryNode.value] += 1;
      });
    }
  }
  return normalize(counts);
}


if (typeof(module) !== 'undefined') {
  module.exports = {
    Node: Node,
    CPT: CPT,
    BayesNet: BayesNet,
    gibbs_ask: gibbs_ask,
  };
}

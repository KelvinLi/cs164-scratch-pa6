'use strict';

var _ = require('./underscore.js');


if (typeof(module) !== 'undefined') {
  var gibbs = require('./gibbs.js');
}

function report_result (expect, result) {
    console.log("Expect: ".concat(expect));
    console.log("Got: ".concat(JSON.stringify(result)));
    console.log("");
}


function test() { 
  var c = new gibbs.Node("C", [0, 1]);
  var s = new gibbs.Node("S", [0, 1]);
  var r = new gibbs.Node("R", [0, 1]);
  var w = new gibbs.Node("W", [0, 1]); 

  var cptC = new gibbs.CPT(c, []);
  var cptS = new gibbs.CPT(s, [c]);
  var cptR = new gibbs.CPT(r, [c]);
  var cptW = new gibbs.CPT(w, [s, r]);

  cptC.add({}, {0: 0.5, 1: 0.5});  
  cptS.add({"C": 1}, {0: 0.9, 1: 0.1});
  cptS.add({"C": 0}, {0: 0.5, 1: 0.5});
  cptR.add({"C": 1}, {0: 0.2, 1: 0.8});
  cptR.add({"C": 0}, {0: 0.8, 1: 0.2});
  cptW.add({"S": 1, "R": 1}, {0: 0.01, 1: 0.99});
  cptW.add({"S": 1, "R": 0}, {0: 0.1, 1: 0.9});
  cptW.add({"S": 0, "R": 1}, {0: 0.1, 1: 0.9});
  cptW.add({"S": 0, "R": 0}, {0: 0.99, 1: 0.01});
  
  var bayesNet = new gibbs.BayesNet([c, s, r, w], [cptC, cptS, cptR, cptW]);
  var iterations = 50000;

  console.log("Running ".concat(iterations, " iterations..."));

  var startTime;
  var endTime;
  var cachings = [false, true];
  _.each(cachings, function(caching) {
    console.log("-----------------------------------------");
    startTime = new Date();
    report_result("{0: 0.35, 1: 0.65}", gibbs.gibbs_ask(w, {}, bayesNet, iterations, caching));
    report_result("{0: 0.82, 1: 0.18}", gibbs.gibbs_ask(s, {"R": 1}, bayesNet, iterations, caching));
    report_result("{0: 0.952381, 1: 0.047619}", gibbs.gibbs_ask(c, {"R": 0, "S": 1}, bayesNet, iterations, caching));
    endTime = new Date();
    console.log("With" + function(bool) { return bool ? "" : "out"; }(caching)
      +" caching: " + (endTime - startTime) / 1000 + " seconds\n");
  });
}

test();
